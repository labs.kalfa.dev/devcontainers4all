"""Behave environment hooks
"""


def before_all(context):
    """Before All hook"""
    context.d4c_configuration = {}


def after_scenario(context, unused_scenario):
    """Manage the after scenario"""
    # Close pexpect instances if any
    if hasattr(context, "d4a_container_handle"):
        # it's ok if it's already closed
        context.d4a_container_handle.close()
