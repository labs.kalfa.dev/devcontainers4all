Feature: Produce a Container using Visual Code Studio manifest

  Scenario: basic manifest can create an image
     Given A VSC manifest with no args and post commands
     When A d4a create request is completed
     Then The image is present in the local image repository


  Scenario: basic manifest can open a shell
     Given A VSC manifest with no args and post commands
     When A d4a shell request is completed
     Then It is possible to open a shell

  @wip
  Scenario: repository is mounted
     Given A VSC manifest with no args and post commands
     When A d4a shell request is completed
     Then The repository is mounted in /workspace
