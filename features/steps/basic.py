"""Behave steps
"""
import re
from pathlib import Path

import docker
import pexpect

# pylint: disable=no-name-in-module
from behave import fixture, given, then, use_fixture, when  # type: ignore
from hamcrest import assert_that, equal_to

# pylint: enable=no-name-in-module


# pylint: disable=function-redefined,unused-argument,missing-function-docstring

ROCKYLINUX_SHELL_PROMPT = r"\[root@.* /]# "


@fixture
def docker_client(context):
    return docker.client.from_env()


@given("A VSC manifest with no args and post commands")
def a_basic_vsc_manifest(context):
    this_dir = Path(__file__).parent
    context.d4a_configuration = {}
    context.d4a_configuration["location"] = f"{this_dir}/assets/basic/devcontainer"
    context.d4a_configuration["repository"] = f"{this_dir}/assets/basic/repository"


@when("A d4a {subcommand} request is completed")
def a_dev_container_is_created(context, subcommand):
    location = context.d4a_configuration["location"]
    repository = context.d4a_configuration["repository"]

    d4a = pexpect.spawn(
        f"d4a {subcommand} --location {location} --repository {repository}"
    )
    # This is a creepy expectation on a specific logging output from the technology handler, which needs to return some
    # output to let the world know what image has been created.
    d4a.expect([re.compile(b"Image (.*) created")])
    tag = d4a.match.groups()[0].decode("utf8").strip()

    context.d4a_spawned_command = d4a
    context.d4a_image_tag = tag


@then("The image is present in the local image repository")
def the_image_is_present(context):
    client = use_fixture(docker_client, context)
    # .get raises if image does not exist
    client.images.get(context.d4a_image_tag)


@then("It is possible to open a shell")
def open_a_shell(context):
    d4a = context.d4a_spawned_command
    d4a.expect([ROCKYLINUX_SHELL_PROMPT], timeout=3)
    d4a.sendline("exit 0")
    d4a.close()


@then("The repository is mounted in {directory}")
def repository_is_mounted(context, directory):
    """
    Requires:
     - context.d4a_container_handle: pexpect.Spawn
    """
    d4a = context.d4a_spawned_command
    d4a.expect([ROCKYLINUX_SHELL_PROMPT], timeout=3)
    d4a.sendline(f"if [ -f {directory}/I_EXIST ]; then exit 0; else exit 10; fi")
    d4a.expect(pexpect.EOF)
    d4a.close()

    assert_that(d4a.exitstatus, equal_to(0))
